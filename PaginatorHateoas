<?php

/** Namespace */
namespace AppBundle\Service;

/** Usages */
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Hateoas\Configuration\Route;
use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Hateoas\Representation\PaginatedRepresentation;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaginatorHateoas
 * @package AppBundle\Service
 */
class PaginatorHateoas
{
    /** @var null|\Symfony\Component\HttpFoundation\Request $request */
    protected $request;

    /**
     * PaginatorHateoas constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $route
     * @param array $parameters
     * @param string $embeddedRel Embedded element name
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function withDoctrineAdapter(
        QueryBuilder    $queryBuilder,
        string          $route,
        array           $parameters = [],
        ?string         $embeddedRel = null
    ) : PaginatedRepresentation {
        /** @var Pagerfanta $pager */
        $pager = $this->createPager('querybuilder', $queryBuilder);

        return $this->makePaginator($pager, $route, $parameters, $embeddedRel);
    }

    /**
     * @param array $data
     * @param $route
     * @param array $parameters
     * @param string $embeddedRel Embedded element name
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function withArrayAdapter(
        array   $data,
        string  $route,
        array   $parameters = [],
        ?string $embeddedRel = null
    ) : PaginatedRepresentation {
        /** @var Pagerfanta $pager */
        $pager = $this->createPager('array', $data);

        return $this->makePaginator($pager, $route, $parameters, $embeddedRel);
    }

    /**
     * @param ArrayCollection $data
     * @param $route
     * @param array $parameters
     * @param string $embeddedRel Embedded element name
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function withCollectionAdapter(
        ArrayCollection $data,
        string          $route,
        array           $parameters = [],
        ?string         $embeddedRel = null
    ) : PaginatedRepresentation {
        /** @var Pagerfanta $pager */
        $pager = $this->createPager('collection', $data);

        return $this->makePaginator($pager, $route, $parameters, $embeddedRel);
    }

    /**
     * @param $pager
     * @param $route
     * @param $parameters
     * @param $embeddedRel
     * @return PaginatedRepresentation
     */
    private function makePaginator(Pagerfanta $pager, $route, $parameters, $embeddedRel)
    {
        /** @var PagerfantaFactory $pagerfantaFactory */
        $pagerfantaFactory = new PagerfantaFactory();

        $pager->setMaxPerPage((null !== $this->request) ? $this->request->get('limit', 20) : 20)
            ->setCurrentPage((null !== $this->request) ? $this->request->get('page', 1) : 1);

        /** @var array $parameters */
        $parameters = array_merge($parameters, (null !== $this->request) ? $this->request->query->all() : []);

        if (null === $embeddedRel) {
            $embeddedRel = substr($route, 0, strpos($route, '.'));
        }

        /** @var PaginatedRepresentation $paginator */
        $paginator = $pagerfantaFactory->createRepresentation(
            $pager,
            new Route($route, $parameters),
            new CollectionRepresentation(
                $pager->getCurrentPageResults(),
                $embeddedRel,
                $embeddedRel
            )
        );

        return $paginator;
    }

    /**
     * @param $type
     * @param $data
     * @return Pagerfanta
     */
    private function createPager($type, $data)
    {
        switch ($type) {
            case "collection":
                /** @var ArrayCollection $data */
                $data = $data->toArray();
                //no break
            case "array":
                /** @var array $data */
                $pager = new Pagerfanta(new ArrayAdapter($data));
                break;
            case "querybuilder":
                /** @var QueryBuilder $data */
                $pager = new Pagerfanta(new DoctrineORMAdapter($data));
                break;
            default:
                throw new \InvalidArgumentException(
                    "The type $type is not valid. "
                    . "Please provide a type amongst the following ones: array, collection, querybuilder"
                );
        }

        return $pager;
    }
}