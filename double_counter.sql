SELECT alpha.identifier, calculator.freePlaces
    FROM  alpha
    LEFT JOIN (
        SELECT usageCounter.identifier , CAST((capacityCounter.total_capacity - usageCounter.total_usages) AS CHAR) AS availability
        FROM (
            SELECT alpha.identifier AS identifier, count(gamma.id) AS total_usages
            FROM alpha
            LEFT JOIN a_b ON alpha.id = a_b.alpha_id
            LEFT JOIN beta ON a_b.beta_id=beta.id
            LEFT JOIN gamma ON beta.id = gamma.beta_id
            GROUP BY alpha.id
        ) AS usageCounter
        LEFT JOIN (
            SELECT alpha.identifier AS identifier, SUM(beta.capacity) AS total_capacity
            FROM beta
            JOIN a_b ON a_b.beta_id = beta.id
            JOIN alpha ON a_b.alpha_id = alpha.id
            GROUP BY alpha.id
        ) AS capacityCounter ON usageCounter.identifier = capacityCounter.identifier
    ) calculator ON alpha.identifier = calculator.identifier